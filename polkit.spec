Summary: A toolkit for defining and handling authorizations
Name: polkit
Version: 123
Release: 4%{?dist}
License: LGPLv2+
URL: http://www.freedesktop.org/wiki/Software/polkit
Source0: https://gitlab.freedesktop.org/%{name}/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc-c++ meson autoconf automake libtool
BuildRequires: expat-devel pam-devel gtk-doc intltool
BuildRequires: systemd systemd-devel gobject-introspection-devel
BuildRequires: glib2-devel >= 2.30.0 pkgconfig(duktape)
Requires: dbus polkit-pkla-compat %{name}-libs = %{version}-%{release}
Requires(pre): shadow-utils
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Provides: PolicyKit = 0.11
Provides: polkit-desktop-policy = 0.103
Provides: polkit-js-engine = %{version}-%{release}

%description
Polkit is a toolkit for defining and handling authorizations.
It's used for allowing unprivileged processes to speak to privileged processes.

%package libs
Summary: Libraries for polkit

%description libs
Libraries files for polkit.

%package devel
Summary: Development files and documentation for %{name}
Requires: %{name}-libs = %{version}-%{release}
Requires: glib2-devel
Obsoletes: PolicyKit-devel <= 0.10
Obsoletes: PolicyKit-docs <= 0.10
Provides: PolicyKit-devel = 0.11
Provides: PolicyKit-docs = 0.11

%description devel
Development files for polkit.

%prep
%autosetup

%build
%meson -D authfw=pam \
       -D examples=false \
       -D gtk_doc=true \
       -D introspection=true \
       -D man=true \
       -D session_tracking=libsystemd-login \
       -D tests=false
%meson_build

%install
%meson_install
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
%find_lang polkit-1

%pre
getent group polkitd >/dev/null || groupadd -r polkitd
getent passwd polkitd >/dev/null || useradd -r -g polkitd -d / -s /sbin/nologin -c "User for polkitd" polkitd
exit 0

%post
%systemd_post polkit.service

%preun
%systemd_preun polkit.service

%postun
%systemd_postun_with_restart polkit.service

%files -f polkit-1.lang
%doc COPYING
%{_datadir}/man/man1/*
%{_datadir}/man/man8/*
%{_datadir}/dbus-1/system.d/org.freedesktop.PolicyKit1.conf
%{_datadir}/dbus-1/system-services/*
%dir %{_datadir}/polkit-1/
%dir %{_datadir}/polkit-1/actions
%attr(0700,polkitd,root) %dir %{_datadir}/polkit-1/rules.d
%{_datadir}/polkit-1/actions/org.freedesktop.policykit.policy
%{_datadir}/polkit-1/policyconfig-1.dtd
%{_unitdir}/polkit.service
%dir %{_sysconfdir}/polkit-1
%{_datadir}/polkit-1/rules.d/50-default.rules
%attr(0700,polkitd,root) %dir %{_sysconfdir}/polkit-1/rules.d
%{_sysconfdir}/pam.d/polkit-1
%{_bindir}/pkaction
%{_bindir}/pkcheck
%{_bindir}/pkttyagent
%attr(4755,root,root) %{_bindir}/pkexec
%dir %{_prefix}/lib/polkit-1
%{_prefix}/lib/polkit-1/polkitd
%attr(4755,root,root) %{_prefix}/lib/polkit-1/polkit-agent-helper-1
%{_datadir}/gtk-doc

%files libs
%{_libdir}/lib*.so.*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir
%{_datadir}/gettext/its/polkit.its
%{_datadir}/gettext/its/polkit.loc

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 123-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 123-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 123-2
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 02 2023 kianli <kianli@tencent.com> - 123-1
- Upgrade to 123

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 121-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 121-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Aug 25 2022 TAO WU <tallwu@tencent.com> - 121-1
- Move source code version to 121.

* Tue Jul 5 2022 TAO WU <tallwu@tencent.com> - 0.120-1
- Initial build.
